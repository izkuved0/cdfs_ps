
 <script src="

https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
     
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">

   <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
      <link rel="stylesheet" type="text/css" href="css/style10.css" />
	  <link src="http://bootswatch.com/flatly/bootstrap.css" type="text/css">
	  

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="payroll_index.php">Home</a>
		  
		   <ul class="nav navbar-nav">
               <li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Employee <span class="caret"></span></a>				
			  <ul class="dropdown-menu" role="menu">
			   <li><a href="#">Add Employee</a></li>
                <li><a href="payroll_view.php">View List</a></li>
				</ul>                
            </li>
			<li><a href="#">Payroll</a></li>
        </ul>
				
		  
     <div class="container">
	<div class="row">
		<div class="col-lg-3">
            <div class="input-group custom-search-form">
              <input type="text" class="form-control">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">&nbsp
              <span class="glyphicon glyphicon-search"></span>
             </button>
             </span>
             </div><!-- /input-group -->
        </div>
	</div>
</div>
    </nav>

<div class="container"><div class="row">
	<div class="row">
		
        
        <div class="col-md-14">
       
        <div class="table-responsive">

                
              <table id="mytable" class="table table-bordred table-striped">
                   
                   <thead>
                   
	<a id="add_row" class="btn btn-default pull-right">Add Row</a>
	               <th>Employee ID</th>
                   <th>Name</th>
                   	<th>Position</th>
					<th></th>
					 <th>Status</th>
                      <th>Edit</th>
                       <th>View</th>
                   </thead>
			<tbody>	   
	<tr>
	
	  <td>1</td>
	<td>John Aaron Pascua</td>
	<td>IT Department</td>
	<td><div class="input-group">
					  
	<td><span class="label label-success">Active</span></td>                    
	<td><p><button class="btn btn-primary btn-s" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
    <td><p><button class="btn btn-warning btn-s" data-title="View" data-toggle="modal" data-target="#view"><span class="glyphicon glyphicon-search"></span></button></p></td>
    </tr>
				
  <tr>
  <td>2</td>
    <td>Lizelle Alla</td>
    <td>IT Department</td>
    <td><div class="input-group">
						 
	<td><span class="label label-success">Active</span></td>                    
	<td><p><button class="btn btn-primary btn-s" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
    <td><p><button class="btn btn-warning btn-s" data-title="View" data-toggle="modal" data-target="#view"><span class="glyphicon glyphicon-search"></span></button></p></td>
    </tr>	
	
	 <tr>
  <td>3</td>
    <td>Joseph Mondragon</td>
    <td>IT Department</td>
    <td><div class="input-group">
					  
	<td><span class="label label-default">Inactive</span></td>                    
	<td><p><button class="btn btn-primary btn-s" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
    <td><p><button class="btn btn-warning btn-s" data-title="View" data-toggle="modal" data-target="#view"><span class="glyphicon glyphicon-search"></span></button></p></td>
    </tr>	
	
	 <tr>
  <td>4</td>
    <td>Joseph Mondragon</td>
    <td>IT Department</td>
    <td><div class="input-group">
					 
	<td><span class="label label-default">Inactive</span></td>                    
	<td><p><button class="btn btn-primary btn-s" data-title="Edit" data-toggle="modal" data-target="#edit"  ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
    <td><p><button class="btn btn-warning btn-s" data-title="View" data-toggle="modal" data-target="#view"><span class="glyphicon glyphicon-search"></span></button></p></td>
    </tr>	
	
	
<div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="view" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title custom_align" id="Heading">Employee Info</h4>
      </div>
          <div class="modal-body">
          <div class="form-group">
        <input class="form-control " type="text" disabled>
        </div>
        <div class="form-group">
        
       
        <input class="form-control " type="text" disabled>
        </div>
		<div class="form-group">
        <input class="form-control " type="text"disabled >
        </div>
		<div class="form-group">
        <input class="form-control " type="text" placeholder="Birthdate">
        </div>
		<div class="form-group">
        <input class="form-control " type="text" placeholder="Gender">
        </div>
		<div class="form-group">
        <input class="form-control " type="text" placeholder="Civil Status">
        </div
		<div class="form-group">
        <input class="form-control " type="text" placeholder="Employees Position">
        </div>
       <div class="modal-footer">
    <a href="payroll_employee_page.php" class="btn">Close</a>
        </div>
        
        </div>
      </div>
         
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>
    
    
</tbody>	
   